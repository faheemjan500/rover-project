/*
 * Copyright 2019 Thales Italia spa.
 *
 * This program is not yet licensed and this file may not be used under any circumstance.
 */
package com.technologyconversations.kata.marsrover;

import java.util.List;

public class Coordinates {
  private Direction direction;

  private final Point x;

  private final Point y;
  private List<Obstacle> obstacles;

  public Coordinates(Point x, Point y, Direction direction, List<Obstacle> obstacles) {
    this.x = x;
    this.y = y;
    this.direction = direction;
    this.obstacles = obstacles;
  }

  public String getCurrentPosition() {
    return x.getLocation() + " X " + y.getLocation() + " Y";

  }

  public Direction getDirection() {
    return direction;
  }

  public List<Obstacle> getObstacles() {
    return obstacles;
  }

  public Point getX() {
    return this.x;
  }

  public Point getY() {
    return this.y;
  }

  public void moveAndSetNextPosition(int changePositionByAmount) {
    Integer currentX = getX().getLocation();
    Integer currentY = getY().getLocation();
    if (getDirection() == Direction.SOUTH) {
      currentY = moveAlongVerticalDirection(changePositionByAmount * -1);
    } else if (direction == Direction.WEST) {
      currentX = moveAlongHorizontalDirection(changePositionByAmount * -1);
    } else if (direction == Direction.NORTH) {
      currentY = moveAlongVerticalDirection(changePositionByAmount);
    } else {
      currentX = moveAlongHorizontalDirection(changePositionByAmount);
    }
    x.setLocation(currentX);
    y.setLocation(currentY);
  }

  public Boolean moveBackward() {
    if (!this.isThereObstacleInTheDirectionOfMovement(-1)) {
      moveAndSetNextPosition(-1);
      return true;
    } else {
      return false;
    }
  }

  public Boolean moveForward() {

    if (!this.isThereObstacleInTheDirectionOfMovement(1)) {
      moveAndSetNextPosition(1);
      return true;
    } else {
      return false;
    }
  }

  public void setDirection(Direction direction) {
    this.direction = direction;
  }

  public void setObstacles(List<Obstacle> asList) {
    this.obstacles = asList;
  }

  @Override
  public String toString() {
    return x.getLocation() + " X " + y.getLocation() + " " + direction.getShortName();
  }

  private boolean isThereObstacle(int xAxis, int yAxis) {
    boolean isObstacle = false;
    for (final Obstacle obstacle : this.getObstacles()) {
      if (xAxis == obstacle.getX() && yAxis == obstacle.getY()) {
        isObstacle = true;
        break;
      }
    }
    return isObstacle;
  }

  private Integer moveAlongHorizontalDirection(int changePositionByAmount) {
    if (changePositionByAmount > 0) {
      return x.getForwardLocation();
    } else {
      return x.getBackwardLocation();
    }
  }

  private Integer moveAlongVerticalDirection(int changePositionByAmount) {
    if (changePositionByAmount > 0) {
      return y.getForwardLocation();
    } else {
      return y.getBackwardLocation();
    }
  }

  boolean isThereObstacleInTheDirectionOfMovement(int coordinateOfObstacle) {
    if (getDirection() == Direction.SOUTH) {
      return isThereObstacle(this.getX().getLocation(),
          this.getY().getLocation() - coordinateOfObstacle);
    } else if (direction == Direction.WEST) {
      return isThereObstacle(this.getX().getLocation() - coordinateOfObstacle,
          this.getY().getLocation());
    } else if (direction == Direction.NORTH) {
      return isThereObstacle(this.getX().getLocation(),
          this.getY().getLocation() + coordinateOfObstacle);
    } else {
      return isThereObstacle(this.getX().getLocation() + coordinateOfObstacle,
          this.getY().getLocation());
    }
  }
}
