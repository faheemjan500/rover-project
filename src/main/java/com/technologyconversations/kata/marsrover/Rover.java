/*
 * Copyright 2019 Thales Italia spa.
 *
 * This program is not yet licensed and this file may not be used under any circumstance.
 */
package com.technologyconversations.kata.marsrover;

public class Rover {
  private static final String NOK = "NOK";

  private String position;

  private final Coordinates coordinates;

  public Rover(Coordinates roverCoordinates) {
    this.coordinates = roverCoordinates;
  }

  public Coordinates getCoordinates() {
    return coordinates;
  }

  public String getPosition() {
    return position;
  }

  public void receiveCommands(String commands) throws UnAcceptableCommandException {

    for (int index = 0; index < commands.length(); index++) {
      if (!receiveSingleCommand(commands.charAt(index))) {
        break;
      }
    }
  }

  public Boolean receiveSingleCommand(char command) throws UnAcceptableCommandException {
    final char upperCase = Character.toUpperCase(command);
    switch (upperCase) {
      case 'F':
        return executeMoveForward();
      case 'B':
        return executeMoveBack();
      case 'L':
        executeTurnLeftCommand();
        break;
      case 'R':
        executeTurnRightCommand();
        break;
      default:
        throw new UnAcceptableCommandException();
    }
    return true;
  }

  public void setPosition(String pos) {
    position = pos;
  }

  private Boolean executeMoveBack() {
    if (coordinates.moveBackward()) {
      setPosition(coordinates.getCurrentPosition());
      return true;
    } else {
      setPosition(coordinates.getCurrentPosition() + NOK);
      return false;
    }
  }

  private Boolean executeMoveForward() {

    if (coordinates.moveForward()) {
      setPosition(coordinates.getCurrentPosition());
      return true;
    } else {
      setPosition(coordinates.getCurrentPosition() + NOK);
      return false;
    }
  }

  private void executeTurnLeftCommand() {

    final Direction direction = coordinates.getDirection();
    coordinates.setDirection(direction.antiClockWiseRotation());

  }

  private void executeTurnRightCommand() {

    final Direction direction = coordinates.getDirection();
    coordinates.setDirection(direction.clockWiseRotation());
  }

}