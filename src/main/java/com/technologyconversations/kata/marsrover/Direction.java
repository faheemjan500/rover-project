/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.technologyconversations.kata.marsrover;

public enum Direction {

  NORTH(0, 'N'),
  EAST(1, 'E'),
  SOUTH(2, 'S'),
  WEST(3, 'W');

  private int value;
  private char shortName;

  private Direction(int newValue, char shortNameValue) {
    value = newValue;
    shortName = shortNameValue;
  }

  public Direction antiClockWiseRotation() {
    return values()[(this.getValue() + 3) % 4];

  }

  public Direction clockWiseRotation() {
    return values()[(this.getValue() + 1) % 4];

  }

  public Direction getBackwardDirection() {
    return values()[(this.getValue() + 1) % 4];
  }

  public char getShortName() {
    return shortName;
  }

  public int getValue() {
    return value;
  }
}