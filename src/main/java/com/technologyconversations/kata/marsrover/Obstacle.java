/*
 * Copyright 2019 Thales Italia spa.
 *
 * This program is not yet licensed and this file may not be used under any circumstance.
 */
package com.technologyconversations.kata.marsrover;

public class Obstacle {
  private final int x;
  private final int y;

  public Obstacle(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

}
