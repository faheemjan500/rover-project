/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package com.technologyconversations.kata.marsrover;

public class UnAcceptableCommandException extends Exception {

  public UnAcceptableCommandException() {
    super();
  }
}
