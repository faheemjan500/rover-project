/*
 * Copyright 2019 Thales Italia spa.
 *
 * This program is not yet licensed and this file may not be used under any circumstance.
 */
package com.technologyconversations.kata.marsrover;

public class Point {

  private int location;
  private int maxLocation;

  public Point(int location, int maxLocation) {
    this.location = location;
    this.maxLocation = maxLocation;
  }

  public int getBackwardLocation() {
    if (getLocation() == 0) {
      return getMaxLocation();
    }
    return getLocation() - 1;
  }

  public int getForwardLocation() {

    if (getLocation() == getMaxLocation()) {
      return 0;
    }

    return 1 + getLocation();

  }

  public int getLocation() {
    return location;
  }

  public int getMaxLocation() {
    return maxLocation;
  }

  public void setLocation(int location) {
    this.location = location;
  }

  public void setMaxLocation(int maxLocation) {
    this.maxLocation = maxLocation;
  }

}