/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.technologyconversations.kata.marsrover;

import java.io.File;

public class Practice {

  public static void main(String[] args) {
    final File file = new File("Data.tx");
    System.out.print("My file name is: " + file.getName());

    if (file.canRead()) {
      System.out.println("\nThe file is readable!");
    } else {
      System.out.println("\nThe file is not readable!");
    }
    if (file.canWrite()) {
      System.out.println("\nThe file is Writable!");
    } else {
      System.out.println("\nThe file is not writable!");
    }
    System.out.println("\nThe size of the file is :" + file.length());
  }

}
