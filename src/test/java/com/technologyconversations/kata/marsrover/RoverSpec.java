/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.technologyconversations.kata.marsrover;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Source: http://dallashackclub.com/rover
 * 
 * Develop an api that moves a rover around on a grid. You are given the initial starting point
 * (x,y) of a rover and the direction (N,S,E,W) it is facing. - The rover receives a character array
 * of commands. - Implement commands that move the rover forward/backward (f,b). - Implement
 * commands that turn the rover left/right (l,r). - Implement wrapping from one edge of the grid to
 * another. (planets are spheres after all) - Implement obstacle detection before each move to a new
 * square. If a given sequence of commands encounters an obstacle, the rover moves up to the last
 * possible point and reports the obstacle.
 */
public class RoverSpec {

  private Rover rover;
  private Coordinates roverCoordinates;
  private final Direction direction = Direction.NORTH;
  private Point x;
  private Point y;
  private List<Obstacle> obstacles;

  @Before
  public void beforeRoverTest() {
    x = new Point(1, 9);
    y = new Point(2, 9);
    obstacles = new ArrayList<Obstacle>();
    roverCoordinates = new Coordinates(x, y, direction, obstacles);
    rover = new Rover(roverCoordinates);
  }

  @Test // 1
  public void newInstanceShouldSetRoverCoordinatesAndDirection() {
    assertThat(rover.getCoordinates()).isEqualToComparingFieldByField(roverCoordinates);
  }

  @Test // 12
  public void positionShouldReturnNokWhenObstacleIsFound() throws Exception {
    rover.getCoordinates().setObstacles(
        Arrays.asList(new Obstacle(x.getLocation() + 1, y.getLocation())));
    rover.getCoordinates().setDirection(Direction.EAST);
    rover.receiveCommands("F");
    assertThat(rover.getPosition()).endsWith("NOK");
  }

  @Test // 10
  public void positionShouldReturnXYAndDirection() throws Exception {
    rover.receiveCommands("LFFFRFF");
    assertThat(rover.getPosition()).isEqualTo("8 X 4 Y");
  }

  @Test // 7
  public void receiveCommandShouldWhatFromOneEdgeOfTheGridToAnother() throws Exception {
    final int expected = x.getMaxLocation() + x.getLocation() - 2;
    rover.receiveCommands("LFFF");
    assertThat(rover.getCoordinates().getX().getLocation()).isEqualTo(expected);
  }

  @Test // 8
  public void receiveCommandsShouldBeAbleToReceiveMultipleCommands() throws Exception {
    final int expected = x.getLocation() + 1;
    rover.receiveCommands("RFR");
    assertThat(rover.getCoordinates().getX().getLocation()).isEqualTo(expected);
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.SOUTH);
  }

  @Test // new1
  public void receiveCommandsShouldGoAroundTheObstacle() throws Exception {
    final int expected = x.getLocation() + 1;
    rover.getCoordinates().setObstacles(Arrays.asList(new Obstacle(expected + 1, y.getLocation())));
    rover.getCoordinates().setDirection(Direction.EAST);
    rover.receiveCommands("FRBLFFRFL");
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.EAST);
  }

  @Test // new2
  public void receiveCommandsShouldGoAroundTheObstacle2() throws Exception {
    final int expected = x.getLocation() + 1;
    rover.getCoordinates().setObstacles(Arrays.asList(new Obstacle(expected + 1, y.getLocation())));
    rover.getCoordinates().setDirection(Direction.EAST);
    rover.receiveCommands("FRBLFFRFL");
    assertThat(rover.getPosition()).isEqualTo("4 X 2 Y");
  }

  @Test // new4
  public void receiveCommandsShouldGoAroundTheObstacleAntiClockWise4() throws Exception {
    final int expected = x.getLocation() + 1;
    rover.getCoordinates().setObstacles(Arrays.asList(new Obstacle(expected + 1, y.getLocation())));
    rover.getCoordinates().setDirection(Direction.EAST);
    rover.receiveCommands("RFLFFLF");
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.NORTH);
  }

  @Test // 9
  public void receiveCommandsShouldStopWhenObstacleIsFound() throws Exception {
    final int expected = x.getLocation() + 1;
    rover.getCoordinates().setObstacles(Arrays.asList(new Obstacle(expected + 1, y.getLocation())));
    rover.getCoordinates().setDirection(Direction.EAST);
    rover.receiveCommands("FFFRF");
    assertThat(rover.getCoordinates().getX().getLocation()).isEqualTo(expected);
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.EAST);
  }

  @Test // new3
  public void receiveCommandsShouldTurnAround3() throws Exception {
    final int expected = x.getLocation() + 1;
    rover.getCoordinates().setObstacles(Arrays.asList(new Obstacle(expected + 1, y.getLocation())));
    rover.getCoordinates().setDirection(Direction.EAST);
    rover.receiveCommands("RRRLL");
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.SOUTH);

  }

  @Test // 2
  public void receiveSingleCommandShouldIgnoreCase() throws Exception {
    rover.receiveSingleCommand('r');
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.EAST);
  }

  @Test // 2b
  public void receiveSingleCommandShouldIgnoreCaseB() throws Exception {
    rover.receiveSingleCommand('R');
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.EAST);
  }

  @Test // 3
  public void receiveSingleCommandShouldMoveBackwardWhenCommandIsB() throws Exception {
    final int expected = y.getLocation() - 1;
    rover.receiveSingleCommand('b');
    assertThat(rover.getCoordinates().getY().getLocation()).isEqualTo(expected);
  }

  @Test // 4
  public void receiveSingleCommandShouldMoveForwardWhenCommandIsF() throws Exception {
    final int expected = y.getLocation() + 1;
    rover.receiveSingleCommand('F');
    assertThat(rover.getCoordinates().getY().getLocation()).isEqualTo(expected);
  }

  // 11**************************
  @Test(expected = Exception.class)
  public void receiveSingleCommandShouldThrowExceptionWhenCommandIsUnknown() throws Exception {
    rover.receiveSingleCommand('X');
  }

  @Test // 6
  public void receiveSingleCommandShouldTurnLeftWhenCommandIsL() throws Exception {
    rover.receiveSingleCommand('L');
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.WEST);
  }

  @Test // 5
  public void receiveSingleCommandShouldTurnRightWhenCommandIsR() throws Exception {
    rover.receiveSingleCommand('R');
    assertThat(rover.getCoordinates().getDirection()).isEqualTo(Direction.EAST);
  }

}