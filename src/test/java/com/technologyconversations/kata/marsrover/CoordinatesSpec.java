/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.technologyconversations.kata.marsrover;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

public class CoordinatesSpec {

  private Coordinates coordinates;
  private Point x;
  private Point y;
  private List<Obstacle> obstacles;
  private final Direction direction = Direction.NORTH;

  @Before
  public void beforeCoordinatesTest() {
    x = new Point(1, 99);
    y = new Point(2, 99);
    obstacles = Arrays.asList(new Obstacle(20, 20), new Obstacle(30, 30));
    coordinates = new Coordinates(x, y, direction, obstacles);
  }

  @Test // 13
  public void moveBackwardShouldDecreaseXWhenDirectionIsEast() {
    final Point expected = new Point(x.getLocation() - 1, x.getMaxLocation());
    coordinates.setDirection(Direction.EAST);
    coordinates.moveBackward();
    assertThat(coordinates.getX()).isEqualToComparingFieldByField(expected);
  }

  @Test // 12
  public void moveBackwardShouldDecreaseYWhenDirectionIsNorth() {
    final Point expected = new Point(y.getLocation() - 1, y.getMaxLocation());
    coordinates.setDirection(Direction.NORTH);
    coordinates.moveBackward();
    assertThat(coordinates.getY()).isEqualToComparingFieldByField(expected);
  }

  @Test // 11
  public void moveBackwardShouldIncreaseXWhenDirectionIsWest() {
    final Point expected = new Point(x.getLocation() + 1, x.getMaxLocation());
    coordinates.setDirection(Direction.WEST);
    coordinates.moveBackward();
    assertThat(coordinates.getX()).isEqualToComparingFieldByField(expected);
  }

  @Test // 10
  public void moveBackwardShouldIncreaseYWhenDirectionIsSouth() {
    final Point expected = new Point(y.getLocation() + 1, y.getMaxLocation());
    coordinates.setDirection(Direction.SOUTH);
    coordinates.moveBackward();
    assertThat(coordinates.getY()).isEqualToComparingFieldByField(expected);
  }

  @Test // 7b
  public void moveBackwardShouldNotChangeLocationsWhenObstacleIsFound() {
    final int expected = x.getLocation();
    coordinates.setDirection(Direction.WEST);
    coordinates.setObstacles(Arrays.asList(new Obstacle(x.getLocation() + 1, y.getLocation())));
    coordinates.moveBackward();
    assertThat(coordinates.getX().getLocation()).isEqualTo(expected);
  }

  @Test // 9
  public void moveForwardShouldDecreaseXWhenDirectionIsWest() {
    final Point expected = new Point(x.getLocation() - 1, x.getMaxLocation());
    coordinates.setDirection(Direction.WEST);
    coordinates.moveForward();
    assertThat(coordinates.getX()).isEqualToComparingFieldByField(expected);
  }

  @Test // 8
  public void moveForwardShouldDecreaseYWhenDirectionIsSouth() {
    final Point expected = new Point(y.getLocation() - 1, y.getMaxLocation());
    coordinates.setDirection(Direction.SOUTH);
    coordinates.moveForward();
    assertThat(coordinates.getY()).isEqualToComparingFieldByField(expected);
  }

  @Test // 6
  public void moveForwardShouldIncreaseXWhenDirectionIsEast() {
    final Point expected = new Point(x.getLocation() + 1, x.getMaxLocation());
    coordinates.setDirection(Direction.EAST);
    coordinates.moveForward();
    assertThat(coordinates.getX()).isEqualToComparingFieldByField(expected);
  }

  @Test // 5
  public void moveForwardShouldIncreaseYWhenDirectionIsNorth() {
    final Point expected = new Point(y.getLocation() + 1, y.getMaxLocation());
    coordinates.setDirection(Direction.NORTH);
    coordinates.moveForward();
    assertThat(coordinates.getY()).isEqualToComparingFieldByField(expected);
  }

  @Test // 7
  public void moveForwardShouldNotChangeLocationsWhenObstacleIsFound() {
    final int expected = x.getLocation();
    coordinates.setDirection(Direction.EAST);
    coordinates.setObstacles(Arrays.asList(new Obstacle(x.getLocation() + 1, y.getLocation())));
    coordinates.moveForward();
    assertThat(coordinates.getX().getLocation()).isEqualTo(expected);
  }

  @Test // 3
  public void newInstanceShouldSetDirection() {
    assertThat(coordinates.getDirection()).isEqualTo(direction);
  }

  @Test // 2
  public void newInstanceShouldSetObstacles() {
    assertThat(coordinates.getObstacles()).hasSameElementsAs(obstacles);
  }

  @Test // 1
  public void newInstanceShouldSetXAndYParams() {
    assertThat(coordinates.getX()).isEqualToComparingFieldByField(x);
    assertThat(coordinates.getY()).isEqualToComparingFieldByField(y);
  }

  @Test // 4
  public void toStringShouldReturnXAndY() {
    final String expected =
        x.getLocation() + " X " + y.getLocation() + " " + direction.getShortName();
    assertThat(coordinates.toString()).isEqualTo(expected);
  }

}