/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.technologyconversations.kata.marsrover;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/*
 * Point is a two-dimensional point on the grid. New instance is created with initial location and
 * maximum location that can be reached on that axis. Methods forward/backward increase/decrease
 * location. If maximum location is reached, forward/backward methods wrap location.
 */
public class PointSpec {

  Point point;
  private final int location = 5;
  private final int maxLocation = 9;

  @Before
  public void beforePointTest() {
    point = new Point(location, maxLocation);
  }

  @Test // 3
  public void getBackwardLocationShouldDecreasePointValueByOne() {
    final int expected = point.getLocation() - 1;
    assertThat(point.getBackwardLocation()).isEqualTo(expected);
  }

  @Test // 4
  public void getBackwardLocationShouldSetValueToMaxLocationIfZeroLocationIsPassed() {
    point.setLocation(0);
    assertThat(point.getBackwardLocation()).isEqualTo(point.getMaxLocation());
  }

  @Test // 2
  public void getForwardLocationShouldIncreasePointValueByOne() {
    final int expected = point.getLocation() + 1;
    assertThat(point.getForwardLocation()).isEqualTo(expected);
  }

  @Test // 5
  public void getForwardLocationShouldSetValueToZeroIfMaxLocationIsPassed() {
    point.setLocation(point.getMaxLocation());
    assertThat(point.getForwardLocation()).isZero();
  }

  @Test // 1
  public void newInstanceShouldSetLocationAndMaxLocationParams() {
    assertThat(point.getLocation()).isEqualTo(location);
    assertThat(point.getMaxLocation()).isEqualTo(maxLocation);
  }

}